package data.functionality;

import java.io.Serializable;


public class Column implements Serializable {
	
	private static final long serialVersionUID = 4371919793121624587L;
	private String name;
	private DataType columnDataType;
	
	
	public Column(String name, DataType dataType) {
		this.name = name;
		this.columnDataType = dataType;
	}
	
	public DataType getDataType() {
		return columnDataType;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean equals(Column column){
		return this.getName().equals(column.getName());
	}
	
	public int hashCode() {
		return this.getName().hashCode();
	}
	
	public String toString(){
		return this.name;
	}

}
