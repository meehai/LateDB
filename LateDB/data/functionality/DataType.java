package data.functionality;


public enum DataType {
	INTEGER,
	DOUBLE,
	STRING,
	BOOLEAN
}
