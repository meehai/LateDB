package data.functionality;

import java.io.Serializable;
import java.util.ArrayList;


public class Database implements Serializable {
	
	private static final long serialVersionUID = -4754740550722032134L;
	private String name;
	private ArrayList<Table> tables;
	
	
	public Database(String name) {
		this.name = name;
		this.tables = new ArrayList<Table>();
	}
	
	
	public void addTable(String name) {
		tables.add(new Table(name));
	}
	
	
	//metoda ce intoarce tabela din baza de date cu numele trimis ca parametru
	public Table getTable(String name) {
		for (Table table : tables)
			if (table.getName().equals(name))
				return table;
		
		return null;
	}
	
	public Table join(String tables) {
		String[] tablesJoin = tables.split(", ");
		Table newTable = null;
		
		if(tablesJoin.length == 1)
			return this.getTable(tablesJoin[0]).deepCloneTable();
		
		int i = 0;
		/* Creez o tabela de tipul tutor coloanelor din tabelele cerute */
		for (i=0; i<tablesJoin.length; i++){
			String table = tablesJoin[i];
			
			Table t = this.getTable(table);
			
			if(i == 0){
				newTable = t.deepCloneTable();
				continue;
			}
			
			int currentLineCount = newTable.getLineCount();
			int newLineCount = t.getLineCount();
						
			//Adaug coloanele noi + diferentiez prin numeTabela.numeColoana
			for (Column c : t.getColumns())
				newTable.addColumn(t.getName() + "." + c.getName(), c.getDataType());
			
			
			for(int j=0; j<currentLineCount; j++){
				/* Salvez o copie a liniei cu care fac produsul vectorial.
				 * Aceasta copie va fi mereu prima linie din tabela veche, la care adaug noile elemente, apoi o sterg.
				 */
				ArrayList<Object> oldLine = newTable.getLine(0);
				for(int k=0; k<newLineCount; k++){
					ArrayList<Object> newLine = newTable.deepCloneLine(oldLine);
					newLine.addAll(t.deepCloneLine(t.getLine(k)));
					newTable.insert(newLine);
				}
				
				newTable.removeLine(0);
			}
		
		}
		
		return newTable;
	}
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	public ArrayList<Table> getTables() {
		return tables;
	}

}
