package data.user_interface.panels;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import data.functionality.Column;
import data.functionality.DataType;
import data.functionality.Table;
import data.user_interface.LateDB;


@SuppressWarnings("serial")
public class CenterPanel extends JPanel {
	
	private LateDB window;
	
	private JPanel tablesPanel, centerNorthPanel = new JPanel();
	private JScrollPane tablesPanelScroll;
	private GridBagConstraints gbc;
	
	private ArrayList<Table> tables;
	private ArrayList<JLabel> tableLabels;
	private ArrayList<JButton> insertButtons, selectButtons, updateButtons, deleteButtons, truncateButtons, removeButtons;
	
	private Object[] options1 = {"Da", "Nu"};
	private Object[] options2 = {"Ok", "Renunță"};
	private JLabel textNoSelectedDatabase, textNoTables;
	private int index;
	
	
	public CenterPanel(LateDB window) {
		super(new BorderLayout());
		this.window = window;
		
		tableLabels = new ArrayList<JLabel>();
		insertButtons = new ArrayList<JButton>();
		selectButtons = new ArrayList<JButton>();
		updateButtons = new ArrayList<JButton>();
		deleteButtons = new ArrayList<JButton>();
		truncateButtons = new ArrayList<JButton>();
		removeButtons = new ArrayList<JButton>();
		
		setPanel();
	}
	
	
	private void setPanel() {
		setBorder(new EmptyBorder(10, 0, 0, 0));
		
		tablesPanel = new JPanel(new GridBagLayout());
		gbc = new GridBagConstraints();
		
		textNoSelectedDatabase = new JLabel("<html><span style='font-size: 16px; font-weight: bold;'>Nicio bază de date selectată!</span></html>", JLabel.CENTER);
		textNoTables = new JLabel("<html><span style='font-size: 14px; font-weight: bold;'>Nicio tabelă existentă!</span></html>", JLabel.CENTER);
		
		//initial nu este nicio baza de date selectata
		add(textNoSelectedDatabase, BorderLayout.CENTER);
		
		JButton addButton = new JButton("<html><b>Adaugă tabelă</b></html>", new ImageIcon("data/images/add.png"));
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JLabel nameLabel = new JLabel("<html><span style='font-size: 12px;'>Numele tabelei:</span></html>");
				JTextField nameField = new JTextField(30);
				
				JLabel columnsLabel = new JLabel("<html><span style='font-size: 12px;'>Coloanele tabelei în format (<b>numeColoana1 tipDate1, numeColoana2 tipDate2, etc.</b>):</span></html>");
				JTextField columnsField = new JTextField(30);
				
				JPanel auxPanel = new JPanel(new GridBagLayout());
				GridBagConstraints gbc = new GridBagConstraints();
				
				gbc.gridx = 0;
				gbc.gridy = 0;
				auxPanel.add(nameLabel, gbc);
				gbc.gridx = 1;
				auxPanel.add(nameField, gbc);
				
				gbc.gridx = 0;
				gbc.gridy = 1;
				auxPanel.add(columnsLabel, gbc);
				gbc.gridx = 1;
				auxPanel.add(columnsField, gbc);
				
				int YES_OPTION = 0, click = JOptionPane.showOptionDialog(window, auxPanel, "LateDB", 
																		 JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options2, options2[0]);
					
				if (click == YES_OPTION) {
					//vedem care sunt proprietatile tabelei de adaugat
					String name = nameField.getText();
					String columns = columnsField.getText();
					Table table = null;
					String errorColumn = null;
					
					if (name != null && !name.equals("")) {
						//vedem sa nu adaugam o tabela cu nume duplicat
						for (int i = 0; i < tables.size(); i ++)
							if (tables.get(i).getName().equals(name)) {
								JOptionPane.showMessageDialog(window, "Există deja o tabelă cu același nume!", "LateDB", JOptionPane.ERROR_MESSAGE);
								return;
							}
					
						table = new Table(name);
					}
					
					if (columns != null && !columns.equals("")) {
						String[] tokens = columns.split(", ");
						
						for (int i = 0; i < tokens.length; i ++) {
							String[] column = tokens[i].split(" ");
							column[1] = column[1].toUpperCase();
							
							if (column[1].equals("INTEGER") || column[1].equals("INT") || column[1].equals("NUMBER"))
								table.addColumn(column[0], DataType.INTEGER);
							else if (column[1].equals("DOUBLE") || column[1].equals("FLOAT") || column[1].equals("REAL") )
								table.addColumn(column[0], DataType.DOUBLE);
							else if (column[1].equals("STRING") || column[1].equals("VARCHAR"))
								table.addColumn(column[0], DataType.STRING);
							else if(column[1].equals("BOOLEAN") || column[1].equals("BOOL"))
								table.addColumn(column[0], DataType.BOOLEAN);
							else {
								errorColumn = column[1];
								table = null;
								break;
							}
						}
					}
					
					/* Verificam daca parsarea tipurilor a decurs cu succes */
					if(table == null){
						JOptionPane.showMessageDialog(window, "Tabela nu a fost adaugată, " + errorColumn + " nu este un tip valid.", "LateDB", JOptionPane.INFORMATION_MESSAGE);
						return;
					}
					
					addTableOnScreen(name);
					tables.add(table);
					JOptionPane.showMessageDialog(window, "Tabela s-a adăugat cu succes!", "LateDB", JOptionPane.INFORMATION_MESSAGE);
		
				}
			}
		});
		centerNorthPanel.add(addButton);
	}
	
	
	public void populatePanel(String selectedDatabaseName) {
		//stergem ce era inainte pe fereastra
		removeAll();
		tablesPanel.removeAll();
		tableLabels.clear();
		insertButtons.clear();
		selectButtons.clear();
		updateButtons.clear();
		deleteButtons.clear();
		truncateButtons.clear();
		removeButtons.clear();
		
		//readaugam panel-ul cu butonul de adauga tabela
		add(centerNorthPanel, BorderLayout.NORTH);
		
		tablesPanelScroll = new JScrollPane(tablesPanel);
		add(tablesPanelScroll, BorderLayout.CENTER);
		
		//extragem tabelele ce se afla la momentul curent in baza de date
		tables = window.getTables(selectedDatabaseName);
		
		gbc.anchor = GridBagConstraints.PAGE_START;
		gbc.weightx = 1;
		gbc.weighty = 1;
		
		//daca exista tabele in baza de date selectata, atunci se vor pune si pe interfata grafica
		if (tables.size() == 0)
			tablesPanel.add(textNoTables);
		else
			for (int i = 0; i < tables.size(); i ++)
				addTableOnScreen(tables.get(i).getName());
		
		window.invalidate();
		window.revalidate();
		window.repaint();
	}
	
	
	private void addTableOnScreen(String tableName) {
		//daca text-ul cu baza de date nu are nicio tabela se afla pe interfata grafica, atunci il vom scoate si setam index-ul (care ne va ajuta la aranjarea obiectelor pe ecran)
		if (tablesPanel.isAncestorOf(textNoTables)) {
			tablesPanel.remove(textNoTables);
			index = 0;
		}
		
		//numele tabelei
		JLabel labelName = new JLabel("<html><span style='font-size: 12px; font-weight: bold;'>" + tableName + "</span></html>", new ImageIcon("data/images/table.jpg"), JLabel.CENTER);
		gbc.gridx = 0;
		gbc.gridy = index;
		tableLabels.add(labelName);
		tablesPanel.add(labelName, gbc);
		
		//butonul pentru operatia de insert in tabela
		JButton insert = new JButton("inserează");
		gbc.gridx = 1;
		insert.addActionListener(new InsertOperation());
		insertButtons.add(insert);
		tablesPanel.add(insert, gbc);
		
		//butonul pentru operatia de select
		JButton select = new JButton("selectează");
		gbc.gridx = 2;
		select.addActionListener(new SelectOperation());
		selectButtons.add(select);
		tablesPanel.add(select, gbc);
		
		//butonul pentru operatia de update
		JButton update = new JButton("updatează");
		gbc.gridx = 3;
		update.addActionListener(new UpdateOperation());
		updateButtons.add(update);
		tablesPanel.add(update, gbc);
		
		//butonul pentru operatia de delete
		JButton delete = new JButton("șterge");
		gbc.gridx = 4;
		delete.addActionListener(new DeleteOperation());
		deleteButtons.add(delete);
		tablesPanel.add(delete, gbc);
		
		//butonul pentru operatia de truncate
		JButton truncate = new JButton("trunchiază");
		gbc.gridx = 5;
		truncate.addActionListener(new TruncateOperation());
		truncateButtons.add(truncate);
		tablesPanel.add(truncate, gbc);
		
		//butonul pentru operatia de stergere tabela
		JButton remove = new JButton("elimină tabelă");
		gbc.gridx = 6;
		remove.addActionListener(new RemoveOperation());
		removeButtons.add(remove);
		tablesPanel.add(remove, gbc);
		
		index ++;
		
		window.invalidate();
		window.revalidate();
		window.repaint();
	}
	
	
	//metoda ce se apeleaza cand se sterge o baza de date
	public void resetPanel() {
		removeAll();
		add(textNoSelectedDatabase, BorderLayout.CENTER);
		
		window.invalidate();
		window.revalidate();
		window.repaint();
	}
	
	
	//clasa pentru operatia de insert intr-o tabela
	class InsertOperation implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			//cerem utilizatorului sa introduca informatiile din noua linie din baza de date
			String data = (String)JOptionPane.showInputDialog(window, new JLabel("<html><span style='font-size: 12px;'>Introduceți datele în format (<b>valoare1 valoare2 etc.</b>):</span></html>"),
					"LateDB", JOptionPane.PLAIN_MESSAGE, null, null, null);
			
			ArrayList<Object> line = new ArrayList<Object>();
			String[] values = data.split(" ");
			
			//vom forma un ArrayList de obiecte din input-ul dat de utilizator
			int i;
		
			//vedem pe care din butoanele insert de pe interfata s-a efectuat click (pe butonul carui tabela)
			for (i = 0; i < insertButtons.size(); i ++)
				if (insertButtons.get(i) == e.getSource())
					break;
			
			Table table = tables.get(i);
			
			for (i = 0; i < values.length; i ++)
				line.add(Table.getCorrectValue(table.getColumnsDataTypes().get(i), values[i]));
			
		
			table.insert(line);
			JOptionPane.showMessageDialog(window, "Operația s-a realizat cu succes!", "LateDB", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	
	//clasa pentru operatia de select dintr-o tabela
	class SelectOperation implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			JLabel columnsLabel = new JLabel("<html><span style='font-size: 12px;'>Coloanele după care se face selecția în format (<b>numeColoana1, numeColoana2, etc.</b>):</span></html>");
			JTextField columnsField = new JTextField(30);
			
			JLabel conditionLabel = new JLabel("<html><span style='font-size: 12px;'>Condiția după care se face select-ul:</span></html>");
			JTextField conditionField = new JTextField(30);
			
			JPanel auxPanel = new JPanel(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
			
			gbc.gridx = 0;
			gbc.gridy = 0;
			auxPanel.add(columnsLabel, gbc);
			gbc.gridx = 1;
			auxPanel.add(columnsField, gbc);
			
			gbc.gridx = 0;
			gbc.gridy = 1;
			auxPanel.add(conditionLabel, gbc);
			gbc.gridx = 1;
			auxPanel.add(conditionField, gbc);
			
			//cerem utilizatorului sa introduca informatiile dupa care se face select
			int click = JOptionPane.showOptionDialog(window, auxPanel, "LateDB", 
													 JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options2, options2[0]);
				
			if (click == 0) {
				int i;
				//vedem pe care din butoanele select de pe interfata s-a efectuat click (pe butonul carui tabela)
				for (i = 0; i < selectButtons.size(); i ++)
					if (selectButtons.get(i) == e.getSource())
						break;
				
				Table select = tables.get(i).select(columnsField.getText()).where(conditionField.getText());
				
				//vom afisa rezultatul ca un tabel
				JTable result = new JTable();
				DefaultTableModel tableModel = new DefaultTableModel();
				result.setModel(tableModel);
				
				//ne folosim tot de panelul auxiliar creat mai sus pentru fereastra de dialog
				auxPanel.removeAll();
				auxPanel.add(new JScrollPane(result));
				
				//adaugam liniile din tabela selectata
				
				for(Column c : select.getColumns())
					tableModel.addColumn(c.getName());
				
				for(i = 0; i < select.getLineCount(); i++)
					tableModel.addRow(select.getLine(i).toArray());
				
				
				JOptionPane.showMessageDialog(window, auxPanel, "LateDB", JOptionPane.PLAIN_MESSAGE);
			}
		}
	}
	
	
	//clasa pentru operatia de update dintr-o tabela
	class UpdateOperation implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			JLabel updatesLabel = new JLabel("<html><span style='font-size: 12px;'>Valorile de updatat din tabelă în format (<b>numeColoana1=nouaValoare1, numeColoana2=nouaValoare2, etc.</b>):</span></html>");
			JTextField updatesField = new JTextField(30);
			
			JLabel conditionLabel = new JLabel("<html><span style='font-size: 12px;'>Condiția după care se face update-ul:</span></html>");
			JTextField conditionField = new JTextField(30);
			
			JPanel auxPanel = new JPanel(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
			
			gbc.gridx = 0;
			gbc.gridy = 0;
			auxPanel.add(updatesLabel, gbc);
			gbc.gridx = 1;
			auxPanel.add(updatesField, gbc);
			
			gbc.gridx = 0;
			gbc.gridy = 1;
			auxPanel.add(conditionLabel, gbc);
			gbc.gridx = 1;
			auxPanel.add(conditionField, gbc);
			
			int click = JOptionPane.showOptionDialog(window, auxPanel, "LateDB", 
													 JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, options2, options2[0]);
				
			if (click == 0) {
				//vedem pe care din butoanele update de pe interfata s-a efectuat click (pe butonul carui tabela)
				int i;
				for (i = 0; i < updateButtons.size(); i ++)
					if (updateButtons.get(i) == e.getSource())
						break;
				
				//realizam operatia de update si schimbam tabela din lista de tabele ale bazei de date
				tables.set(i, tables.get(i).update(updatesField.getText(), conditionField.getText()));
				JOptionPane.showMessageDialog(window, "Operația s-a realizat cu succes!", "LateDB", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
	
	
	//clasa pentru operatia de delete dintr-o baza de date
	class DeleteOperation implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String condition = (String)JOptionPane.showInputDialog(window, "Se vor șterge liniile din tabelă care îndeplinesc următoarea condiție:",
					   "LateDB", JOptionPane.PLAIN_MESSAGE, null, null, null);

			//vedem pe care din butoanele delete de pe interfata s-a efectuat click (pe butonul carui tabela)
			int i;
			for (i = 0; i < deleteButtons.size(); i ++)
				if (deleteButtons.get(i) == e.getSource())
					break;
			
			//realizam operatia de delete si schimbam tabela din lista de tabele ale bazei de date
			tables.set(i, tables.get(i).delete(condition));
			JOptionPane.showMessageDialog(window, "Operația s-a realizat cu succes!", "LateDB", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	
	//clasa pentru operatia de truncate dintr-o baza de date
	class TruncateOperation implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			int click = JOptionPane.showOptionDialog(window,
					"Sunteţi sigur(ă) că doriți să trunchiați tabela?", "LateDB", 
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, 
					null, options1, options1[0]);
				
			if (click == 0) {
				//vedem pe care din butoanele truncate de pe interfata s-a efectuat click (pe butonul carui tabela)
				int i;
				for (i = 0; i < truncateButtons.size(); i ++)
					if (truncateButtons.get(i) == e.getSource())
						break;
				
				//realizam operatia de truncate si schimbam tabela din lista de tabele ale bazei de date
				tables.set(i, tables.get(i).truncate());
				JOptionPane.showMessageDialog(window, "Operația s-a realizat cu succes!", "LateDB", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
	
	
	//clasa pentru operatia de stergere tabela dintr-o baza de date
	class RemoveOperation implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			int click = JOptionPane.showOptionDialog(window,
					"Sunteţi sigur(ă) că doriți să ștergeți tabela?", "LateDB", 
					JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, 
					null, options1, options1[0]);
				
			if (click == 0) {
				//vedem pe care din butoanele remove de pe interfata s-a efectuat click (pe butonul carui tabela)
				int i;
				for (i = 0; i < removeButtons.size(); i ++)
					if (removeButtons.get(i) == e.getSource())
						break;
				
				//stergem de pe interfata grafica elementele acelei tabele
				tablesPanel.remove(tableLabels.get(i));
				tablesPanel.remove(insertButtons.get(i));
				tablesPanel.remove(selectButtons.get(i));
				tablesPanel.remove(updateButtons.get(i));
				tablesPanel.remove(deleteButtons.get(i));
				tablesPanel.remove(truncateButtons.get(i));
				tablesPanel.remove(removeButtons.get(i));
									
				tableLabels.remove(i);
				insertButtons.remove(i);
				selectButtons.remove(i);
				updateButtons.remove(i);
				deleteButtons.remove(i);
				truncateButtons.remove(i);
				removeButtons.remove(i);
				
				//stergem din baza de date tabela
				tables.remove(i);
				//daca s-a ajuns sa nu mai avem nicio tabela in baza de date, atunci vom afisa doar textul cu baza de date nu are nicio tabela
				if (tables.size() == 0) {
					removeAll();
					
					add(centerNorthPanel, BorderLayout.NORTH);
					
					add(tablesPanelScroll, BorderLayout.CENTER);
					tablesPanel.add(textNoTables);
				}
				
				window.invalidate();
				window.revalidate();
				window.repaint();
				
				JOptionPane.showMessageDialog(window, "Operația s-a realizat cu succes!", "LateDB", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}

}
