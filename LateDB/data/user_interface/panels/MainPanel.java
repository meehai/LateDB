package data.user_interface.panels;

import java.awt.BorderLayout;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import data.user_interface.LateDB;


@SuppressWarnings("serial")
public class MainPanel extends JPanel {
	
	private LateDB window;
	private CenterPanel centerPanel;
	
	private String selectedDatabaseName;
	private DefaultListModel<JLabel> listModel;
	private JButton deleteButton;
	
	
	public MainPanel(LateDB window) {
		super(new BorderLayout());
		this.window = window;
		
		setPanel();
	}
	
	
	private void setPanel() {
		add(new NorthPanel(window, this), BorderLayout.NORTH);
		
		centerPanel = new CenterPanel(window);
		add(centerPanel, BorderLayout.CENTER);
		
		add(new WestPanel(window, this), BorderLayout.WEST);
	}
	
	
	//functie ce populeaza lista de tabele cu tabelele din baza de date avand numele trimis ca parametru
	public void populateCenterPanel(String selectedDabaseName) {
		centerPanel.populatePanel(selectedDabaseName);
	}
	
	
	public void resetCenterPanel() {
		centerPanel.resetPanel();
	}
	
	
	public String getSelectedDatabase() {
		return selectedDatabaseName;
	}
	
	public void setSelectedDatabase(String selectedDabaseName) {
		this.selectedDatabaseName = selectedDabaseName;
	}
	
	
	public DefaultListModel<JLabel> getListModel() {
		return listModel;
	}
	
	public void setListModel(DefaultListModel<JLabel> listModel) {
		this.listModel = listModel;
	}
	
	
	public JButton getDeleteButton() {
		return deleteButton;
	}
	
	public void setDeleteButton(JButton deleteButton) {
		this.deleteButton = deleteButton;
	}

}
